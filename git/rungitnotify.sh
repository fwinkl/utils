#!/usr/bin/env bash

export LC_CTYPE=C  # suppress acron warnings

source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc11-opt/setup.sh

tmpfile=`mktemp` 
hours=24

./git-notify.py -p atlas/athena -lA master -lO Core -lO Trigger -lO NewConfig -lO Build -s $hours --noMail > $tmpfile

./git-notify.py -p atlas/athena -lA 22.0 -lO Core -lO Trigger -lO NewConfig -lO Build -s $hours --noMail >> $tmpfile

./git-notify.py -p gaudi/Gaudi -s $hours --noMail >> $tmpfile

# Send mail if non-zero results
lines=`cat $tmpfile | wc -l`
if [ "$lines" -gt "0" ]; then
    sendfile=`mktemp`
    cat <<EOF > $sendfile
MIME-Version: 1.0
Content-Type: text/html; charset="us-ascii"
Content-Transfer-Encoding: 8bit
Subject: [GitLab] New Trigger MRs
From: frank.winklmeier@cern.ch
To: atlas-trigger-sw-gitnotify@cern.ch
EOF
    cat $tmpfile >> $sendfile
    #/usr/sbin/sendmail atlas-trigger-sw-gitnotify@cern.ch < $sendfile
    /usr/sbin/sendmail fwinkl@cern.ch < $sendfile
    rm $sendfile
fi

rm $tmpfile
#
