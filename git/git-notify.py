#!/usr/bin/env python3
#
# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
"""Send notification email for new merge requests
"""

__author__ = 'Frank Winklmeier'

import sys
import requests
import argparse
import urllib
import datetime as dt

api_url = "https://gitlab.cern.ch/api/v4"

def send(mr, header='', noMail=False):
    import smtplib
    from email.mime.text import MIMEText
    from operator import itemgetter

    me = 'frank.winklmeier@cern.ch'
    txt = '<html><body style="font-family:arial">'
    txt += header + '\n<table>\n'
    txt += '<tr><th align="left" width="75%">Title</th><th align="left">Creator</th></tr>\n'

    for m in sorted(mr, key=itemgetter('created_at')):
        title = m['title']
        txt += '<tr><td><a href="%s">%s</a></td><td>%s</td></tr>\n' % (m['web_url'],title,m['author']['name'])

    txt += '</table><br></body></html>'

    msg = MIMEText(txt, 'html', 'utf-8')
    msg['Subject'] = '[GitLab] New MRs'
    msg['From'] = me
    msg['To'] = me
    if noMail:
        #print(txt.encode('utf-8'))
        print(txt)
    else:
        s = smtplib.SMTP('localhost')
        s.sendmail(msg['From'], msg['To'], msg.as_string())
        s.quit()


def getMRs(args, selection):
    mrs = []
    page = 1
    while True:
        r = requests.get("%s/projects/%s/merge_requests?per_page=100&page=%d&%s" % 
                         (api_url, urllib.parse.quote_plus(args.project), page, selection),
                         headers = {"Private-Token" : args.token})
        mrs += r.json()
        if r.headers['X-Next-Page']=='': break
        page += 1

    return mrs


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    
    parser.add_argument('-p', '--project', action='store', default='atlas/athena',
                        help='GitLab project [%(default)s]')

    parser.add_argument('-t', '--token', action='store',
                        help='GitLab private authorization token')

    parser.add_argument('-s', '--since', action='store', type=int, default=24,
                        help='Notify about new merge requests within SINCE hours')

    parser.add_argument('-lA', '--labelAND', action='append', default=[],
                        help='Merge request labels (AND)')

    parser.add_argument('-lO', '--labelOR', action='append', default=[],
                        help='Merge request labels (OR)')

    parser.add_argument('--noMail', action='store_true', default=False,
                        help='Do not send email, just print')

    parser.add_argument('--debug', action='store_true', default=False,
                        help='Print debugging information')

    args = parser.parse_args()

    # Get since timestamp
    since = (dt.datetime.utcnow()-dt.timedelta(hours=args.since)).strftime('%Y-%m-%dT%H:%M:%S.%f')

    # Retrieve updated and merged MRs
    mrs = getMRs(args, "state=all&created_after=%s" % since)

    # Filter MRs merged since "since"
    mrs_merged = []

    mr = []
    for m in mrs+mrs_merged:
        if args.debug: print('Considering MR ', m['title'], m['labels'], m['created_at'])
        if len(args.labelAND)==0 and len(args.labelOR)==0:
            labelMatch = True
        else:
            labelMatch = set(args.labelAND).issubset(set(m['labels'])) \
                and len(set(args.labelOR).intersection(set(m['labels'])))>0

        if args.debug: print(labelMatch)
        if labelMatch and m['author']['username'] not in ['atnight']:       # ignore sweeps
            mr.append(m)
        
    if len(mr)>0:
        header = f'Merge requests in <b>{args.project}</b> in the last {args.since} hours'
        if args.labelAND:
            header += f' with labels <b>[{", ".join(args.labelAND)}]</b>'
        if args.labelOR:
            header += f' and [{", ".join(args.labelOR)}]'
        header += ':'
        send(mr, header, args.noMail)

if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        sys.exit(1)
