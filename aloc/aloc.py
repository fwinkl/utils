#!/usr/bin/env python3
"""Calculate lines of codes"""

__author__ = "Frank Winklmeier"

import os
import sys
import json
import argparse
import datetime
import operator
import subprocess
import logging
import pickle
from collections import defaultdict, Counter

log = logging.getLogger('aloc')


domains = { 'Total' : ['./'],
            'Core' : ['Control', 'Database', 'DetectorDescription', 'Event', 'Tools'],
            'ID/Tracking' : ['InnerDetector','Tracking'],
            'Calo' : ['Calorimeter', 'LArCalorimeter', 'TileCalorimeter'],
            'MuonSpectrometer' : ['MuonSpectrometer'],
            'Reconstruction' : ['Reconstruction'],
            'Simulation' : ['Simulation'],
            'Trigger' : ['Trigger', 'HLT'],
            'PhysicsAnalysis' : ['PhysicsAnalysis'],
            'Generators' : ['Generators'],
            'Other' : ['AsgExternal', 'AtlasGeometryCommon', 'AtlasTest', 'Build',
                       'Commission', 'DataQuality', 'HighGranularityTimingDetector',
                       'docs', 'External', 'ForwardDetectors', 'LumiBlock',
                       'MagneticField', 'TestBeam', 'graphics']
}


class Ref:
   """Class to hold information for each git commit"""
   def __init__(self, commit, date, tag=None):
      self.commit = commit
      self.date = date
      self.tag = tag
      self.loc = {}


def get_git_refs(branch, since=None):
   """Return all git commits for branch as (commit,date,tag)"""
   p = subprocess.run(["git","log"] + ([f"--since={since}"] if since else []) +
                      ["--pretty=format:%H %cI %d",
                       "--grep=into '" + branch.split('/')[-1] + "'",
                       "--merges", "--decorate", branch],
                      capture_output=True, text=True, check=True)

   refs = []
   for line in p.stdout.splitlines():
      fields = line.strip("'").split(maxsplit=2)
      commit = fields[0]
      date = datetime.datetime.fromisoformat(fields[1])
      tags = []
      if len(fields)>2:
         tags = [t.strip(', ') for t in fields[2].strip('()').split('tag: ') if 'release/' in t]

      refs.append(Ref(commit, date, tags[0] if tags else None))
   return refs


def get_loc(commit, directories=['./']):
   """Count lines of code for commit"""
   subprocess.run(['git', 'checkout', '--quiet', commit],
                  capture_output=True, check=True)

   # Filter directories in case some got deleted:
   dirs = [d for d in directories if os.path.isdir(d)]

   cmd = '/afs/cern.ch/user/f/fwinkl/bin/scc'
   p = subprocess.run([cmd, '-f','json'] + dirs,
                      capture_output=True, text=True, check=True)
   return json.loads(p.stdout)


def digest_loc_json(json):
   """Produce a digested version of the full JSON"""
   digest = {'C++' : 'C/C++',
             'C' : 'C/C++',
             'C++ Header': 'C/C++',
             'C Header': 'C/C++',
             'Python': 'Python',
             #'FORTRAN Legacy' : 'Fortran',
             #'FORTRAN Modern' : 'Fortran',
   }

   locs = defaultdict(Counter)
   for l in json:
      if l['Name'] in digest:
         l.pop('Files')  # ignore this empty list
         locs[digest[l.pop('Name')]] += Counter(l)

   return locs


def main():
   parser = argparse.ArgumentParser(description=__doc__)

   parser.add_argument('branch', nargs='?', default='upstream/main',
                       help='git branch')

   parser.add_argument('--cache', action='store_true',
                       help='use local commit cache')

   parser.add_argument('--since', action='store', metavar='DATE', default=None,
                       help='consider commits since DATE (date format as in git log)')

   parser.add_argument('-i', '--interval', type=int, action='store', metavar='DAYS', default=None,
                       help='step size in DAYS')

   args = parser.parse_args()

   logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
                       level=logging.INFO, stream=sys.stdout)

   refs = {}
   if args.cache:
      # Load cache:
      try:
         with open('aloc.pkl','rb') as f:
            refs = pickle.load(f)
      except Exception as e:
         log.warning('Cannot load aloc.pkl: %s', e)

   d0 = sorted(refs.values(), key=lambda r:r.date)[-1].date if refs else None
   log.info('Latest git reference in cache: %s', d0)

   # Get all commits from branch:
   for r in reversed(get_git_refs(args.branch, args.since)):
      # Interval selection:
      if args.interval is not None and d0 is not None:
         if abs((r.date-d0).days) < args.interval:
            continue
      d0 = r.date
      # Count lines of codes for selected commits:
      if r.commit not in refs:
         log.info('Counting LOC for %s from %s', r.commit, r.date)
         for d in domains:
            r.loc[d] = get_loc(r.commit, domains[d])
         refs[r.commit] = r
      else:
         log.info('LOCs for %s from %s already in cache', r.commit, r.date)

   if args.cache:
      with open('aloc.pkl','wb') as f:
         pickle.dump(refs, f)

   series = defaultdict(lambda : defaultdict(list))  # series['Python']['Trigger'] = [...]
   for r in sorted(refs.values(), key=operator.attrgetter('date')):
      for d in domains:
         loc = digest_loc_json(r.loc[d])
         for lang in loc:
            series[lang][d].append([int(r.date.timestamp())*1000, loc[lang]['Code']])

   # Write Highcharts JSON
   out = defaultdict(list)
   for lang, d in series.items():
      for name, data in d.items():
         out[lang].append({'name' : name,
                           'id': name,
                           'data' : data})

   json.dump(out, open('aloc.json','w'))


if __name__ == '__main__':
   sys.exit(main())
